# Stock_Market
* Virtual stock market where you can buy, sell and track portfolio

# Files
* CountingUsers.txt simply counts the number of users that have created an account
* Database.txt contains all the data
* stockMarket.cpp is the program file, you may compile and run it. Unfortunately, it only runs on windows.

# How to Run
* Type in `make setup`
* Then, simply exceute stockMarket.cpp
* If at any point you want to reset the data, type in `make reset`
